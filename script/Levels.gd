extends Control


signal level1_pressed
signal level2_pressed
signal level3_pressed
signal level4_pressed
signal level5_pressed
signal level6_pressed


# BUILTINS - - - - - - - - -


# METHODS - - - - - - - - -


# SIGNALS - - - - - - - - -


func _on_Btn1_pressed() -> void:
	emit_signal("level1_pressed")


func _on_Btn2_pressed() -> void:
	emit_signal("level2_pressed")


func _on_Btn3_pressed() -> void:
	emit_signal("level3_pressed")


func _on_Btn4_pressed() -> void:
	emit_signal("level4_pressed")


func _on_Btn5_pressed() -> void:
	emit_signal("level5_pressed")


func _on_Btn6_pressed() -> void:
	emit_signal("level6_pressed")
