extends Control


export (PackedScene) var IcPart


signal game_ended


onready var Board: TextureRect = get_node("Board")
onready var MyTime: Label = get_node("Time")
onready var MyTween: Tween = get_node("Tween")


enum { WIN, LOOSE, IDLE }

var level: int # 1, 2, 3, ...
var active_icon: TextureRect = null
var neighbours: Array = []
var time: int setget _set_time, _get_time
var level_state: int = IDLE

# подгруженные текстуры
var textures: = []
# свойства уровней
const PROPS: = [
	{
		count_x = 3,
		count_y = 6,
		w = 160,
		h = 176,
		total = 18,
		time = 90, # время на выполнения уровня в секундах
		board_offset_y = 70,
		board_pad_x = 18,
		board_pad_y = 18,
	},
	{
		count_x = 3,
		count_y = 6,
		w = 160,
		h = 176,
		total = 18,
		time = 80,
		board_offset_y = 70,
		board_pad_x = 18,
		board_pad_y = 18,
	},
	{
		count_x = 3,
		count_y = 6,
		w = 160,
		h = 176,
		total = 18,
		time = 70,
		board_offset_y = 70,
		board_pad_x = 18,
		board_pad_y = 18,
	},
	{
		count_x = 3,
		count_y = 6,
		w = 160,
		h = 176,
		total = 18,
		time = 60,
		board_offset_y = 70,
		board_pad_x = 18,
		board_pad_y = 18,
	},
	{
		count_x = 3,
		count_y = 6,
		w = 160,
		h = 176,
		total = 18,
		time = 50,
		board_offset_y = 70,
		board_pad_x = 18,
		board_pad_y = 18,
	},
	{
		count_x = 3,
		count_y = 6,
		w = 160,
		h = 176,
		total = 18,
		time = 40,
		board_offset_y = 70,
		board_pad_x = 18,
		board_pad_y = 18,
	},
]


# BUILTINS - - - - - - - - -


func _ready() -> void:
	$Result.hide()
	# подгрузка текстур иконок
	for i in PROPS.size():
		textures.append([])
		for j in PROPS[i].total:
			textures[i].append(load("res://assets/image/ic_lvl_%s/%s.png" % [i + 1, j + 1]))


# клик по картинкам
func _gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.pressed and level_state == IDLE:
		var rect: Rect2 = Board.get_rect()
		var cur: Vector2 = get_local_mouse_position() - rect.position
		if cur.x >= 0 and cur.y >= 0 and cur.x <= rect.size.x and cur.y <= rect.size.y:
			find_icon(cur)


# METHODS - - - - - - - - -


func _set_time(new_time: int) -> void:
	time = new_time
	MyTime.text = "Time: %s sec" % new_time

func _get_time() -> int:
	return time


# подготовка уровня
func prepare_level(new_level: int) -> void:
	$Result.hide()
	clear_level()
	level = new_level
	_set_time(PROPS[level - 1].time)
	load_board()
	$Timer.start()


# подгрузка картинок
func load_board() -> void:
	var icons = []
	# генерим иконки
	for i in textures[level - 1].size():
		var icon: TextureRect = IcPart.instance()
		icon.texture = textures[level - 1][i]
		icon.index = i
		icons.append(icon)
	# перемешиваем массив иконок
	icons.shuffle()
	# расставляем на доске
	for i in icons.size():
		icons[i].x = i % PROPS[level - 1].count_x
		icons[i].y = floor(i / PROPS[level - 1].count_x)
		var x: int = PROPS[level - 1].w * icons[i].x + PROPS[level - 1].board_pad_x
		var y: int = PROPS[level - 1].h * icons[i].y + PROPS[level - 1].board_pad_y
		icons[i].set_position(Vector2(x, y))
		Board.add_child(icons[i])
	icons.clear()

	var w: float = PROPS[level - 1].w * PROPS[level - 1].count_x
	var h: float = PROPS[level - 1].h * PROPS[level - 1].count_y
	Board.set_size(Vector2(w, h))

	var pos: Vector2 = (self.get_rect().size - Board.get_rect().size) / 2
	pos.y += PROPS[level - 1].board_offset_y
	Board.set_position(pos)


# очистка уровня
func clear_level() -> void:
	for i in get_tree().get_nodes_in_group("icon_group"):
		i.queue_free()
	Board.set_size(Vector2(0.0, 0.0))
	level_state = IDLE
	$Timer.stop()


# поиск картинки и выделение
func find_icon(cur: Vector2) -> void:
	var x: int = floor(cur.x / PROPS[level - 1].w) as int
	var y: int = floor(cur.y / PROPS[level - 1].h) as int
	for i in get_tree().get_nodes_in_group("icon_group"):
		# активная иконка
		if i.x == x and i.y == y:
			if active_icon == i:
				_unselect_icon()
				return
			elif _is_neighbour(i):
				_move_icons(i)
				_check_pattern()
				_unselect_icon()
				return
			else:
				_unselect_icon()
				active_icon = i
				active_icon.set_as_selected(true)
	if active_icon:
		find_neigbors(x, y)


# нахождение соседей
func find_neigbors(x: int, y: int) -> void:
	for i in get_tree().get_nodes_in_group("icon_group"):
		# левый и правый соседи
		if i.x == x - 1 and i.y == y or i.x == x + 1 and i.y == y:
			i.set_as_neighbour(true)
			neighbours.append(i)
		# верхний и нижний соседи
		if i.x == x and i.y == y - 1 or i.x == x and i.y == y + 1:
			i.set_as_neighbour(true)
			neighbours.append(i)


# снятие выделения иконки
func _unselect_icon() -> void:
	if active_icon:
		active_icon.set_as_selected(false)
		active_icon = null
		for i in neighbours:
			i.set_as_neighbour(false)
		neighbours.clear()


# проверка на клик по соседу
func _is_neighbour(icon: TextureRect) -> bool:
	for i in neighbours:
		if i == icon:
			return true
	return false


# перемещение иконок
func _move_icons(neighbour: TextureRect) -> void:
	var n_pos: Vector2 = neighbour.get_rect().position
	var a_pos: Vector2 = active_icon.get_rect().position
	var n_indexes: Dictionary = { x = neighbour.x, y  = neighbour.y }
	var a_indexes: Dictionary = { x = active_icon.x, y  = active_icon.y }

	active_icon.x = n_indexes.x
	active_icon.y = n_indexes.y
	neighbour.x = a_indexes.x
	neighbour.y = a_indexes.y

	var _ia = MyTween.interpolate_property(active_icon, "rect_position", a_pos, n_pos, 0.2)
	var _in = MyTween.interpolate_property(neighbour, "rect_position", n_pos, a_pos, 0.2)
	if not MyTween.is_active():
		var _s: bool = MyTween.start()


func _check_pattern() -> void:
	var check: bool = true
	var icons = get_tree().get_nodes_in_group("icon_group")
	for i in icons.size():
		var current_i = icons[i].x + icons[i].y * PROPS[level - 1].count_x
		if icons[i].index != current_i:
			check = false
	if check:
		level_state = WIN
	else:
		level_state = IDLE
	game_end()


func game_end() -> void:
	match level_state:
		WIN:
			$Result/AnimationPlayer.play("light_idle")
			$Result/Label.text = "You won!"
			$Timer.stop()
			$Result.show()
			emit_signal("game_ended", level * 10000)
		LOOSE:
			$Result/AnimationPlayer.play("light_idle")
			$Result/Label.text = "You lose!"
			$Result.show()
#		IDLE:
#			$Result.hide()


# SIGNALS - - - - - - - - -


func _on_Timer_timeout() -> void:
	var current_time = _get_time()
	if current_time == 0:
		level_state = LOOSE
		$Timer.stop()
	else:
		_set_time(current_time - 1)
		$Timer.start()
	game_end()
