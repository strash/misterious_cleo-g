extends Control


signal btn_back_pressed


var score: int = 0


# BUILTINS - - - - - - - - -

func _process(_delta: float) -> void:
	$Score/Label.text = str(score)


# METHODS - - - - - - - - -


func show_hide_btn_back(show: bool) -> void:
	var score_pos_x: float = get_rect().size.x - 20.0 - $Score.get_rect().size.x
	match show:
		true:
			$Tween.interpolate_property($BtnBack, "rect_position:x", -200.0, 10.0, 0.2)
			$Tween.interpolate_property($Score, "rect_position:x", 99.0, score_pos_x, 0.2)
		false:
			$Tween.interpolate_property($BtnBack, "rect_position:x", 10.0, -200.0, 0.2)
			$Tween.interpolate_property($Score, "rect_position:x", score_pos_x, 99.0, 0.2)
	if not $Tween.is_active():
		$Tween.start()


func change_score(new_score: int) -> void:
	$Tween.interpolate_property(self, "score", score, score + new_score, 4.0)
	if not $Tween.is_active():
		$Tween.start()

# SIGNALS - - - - - - - - -


func _on_BtnBack_pressed() -> void:
	emit_signal("btn_back_pressed")
