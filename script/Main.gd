extends Node


onready var MyHeader: Control = get_node("Header")
onready var MyLevels: Control = get_node("Levels")
onready var MyGame: Control = get_node("Game")


# BUILTINS - - - - - - - - -


func _ready() -> void:
	var _seed = rand_seed(1)

	var _btn_back_pressed: int = MyHeader.connect("btn_back_pressed", self, "_on_MyHeader_btn_back_pressed")

	var _level1_pressed: int = MyLevels.connect("level1_pressed", self, "_on_MyLevels_level1_pressed")
	var _level2_pressed: int = MyLevels.connect("level2_pressed", self, "_on_MyLevels_level2_pressed")
	var _level3_pressed: int = MyLevels.connect("level3_pressed", self, "_on_MyLevels_level3_pressed")
	var _level4_pressed: int = MyLevels.connect("level4_pressed", self, "_on_MyLevels_level4_pressed")
	var _level5_pressed: int = MyLevels.connect("level5_pressed", self, "_on_MyLevels_level5_pressed")
	var _level6_pressed: int = MyLevels.connect("level6_pressed", self, "_on_MyLevels_level6_pressed")

	var _game_ended: int = MyGame.connect("game_ended", self, "_on_MyGame_game_ended")

	set_view(1)


# METHODS - - - - - - - - -


func set_view(view: int) -> void:
	match view:
		1:
			MyHeader.show_hide_btn_back(false)
			MyLevels.show()
			MyGame.hide()
		2:
			MyHeader.show_hide_btn_back(true)
			MyLevels.hide()
			MyGame.show()


# SIGNALS - - - - - - - - -


func _on_MyHeader_btn_back_pressed() -> void:
	set_view(1)
	MyGame.clear_level()


func _on_MyLevels_level1_pressed() -> void:
	MyGame.prepare_level(1)
	set_view(2)


func _on_MyLevels_level2_pressed() -> void:
	MyGame.prepare_level(2)
	set_view(2)


func _on_MyLevels_level3_pressed() -> void:
	MyGame.prepare_level(3)
	set_view(2)


func _on_MyLevels_level4_pressed() -> void:
	MyGame.prepare_level(4)
	set_view(2)


func _on_MyLevels_level5_pressed() -> void:
	MyGame.prepare_level(5)
	set_view(2)


func _on_MyLevels_level6_pressed() -> void:
	MyGame.prepare_level(6)
	set_view(2)


func _on_MyGame_game_ended(new_score: int) -> void:
	$Header.change_score(new_score)
